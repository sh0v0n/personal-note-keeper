import "package:flutter/material.dart";
import 'package:personal_note_keeper/screens/note_detail.dart';
import 'package:personal_note_keeper/screens/note_list.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return MaterialApp(
      title: "My Personal Note Keeper",
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.deepPurple
      ),
      home: NoteDetail(),
    );
  }

}