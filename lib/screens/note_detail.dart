import 'package:flutter/material.dart';

class NoteDetail extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return NoteDetailState();
  }

}

class NoteDetailState extends State<NoteDetail>{
  List<String> _priorityList = ['High', 'Medium', 'Low'];
  String _selectedPriority;

  TextEditingController titleController = TextEditingController();
  TextEditingController descriptionController = TextEditingController();

  @override
  void initState(){
    super.initState();
    _selectedPriority = _priorityList[0];
  }

  @override
  Widget build(BuildContext context) {
    TextStyle textStyle = Theme.of(context).textTheme.title;

    return Scaffold(
      appBar: AppBar(
        title: Text("Note Header"),
      ),
      body: Padding(
        padding: EdgeInsets.all(10),
        child: ListView(
          children: <Widget>[
            DropdownButton(
              items: _priorityList.map((String dropdownItem){
                return DropdownMenuItem(
                  value: dropdownItem,
                  child: Text(dropdownItem),
                );
              }).toList(),
              onChanged: (valueSelectedByUser){
                setState(() {
                  _selectedPriority = valueSelectedByUser;
                  debugPrint("Priority selected $valueSelectedByUser");
                });
              },
              value: _selectedPriority,
            ),

            Padding(
              padding: EdgeInsets.only(top: 10, bottom: 10),
              child: TextField(
                controller: titleController,
                style: textStyle,
                decoration: InputDecoration(
                  labelText: 'Title',
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(5)
                  )
                ),
                onChanged: (value){
                  debugPrint("Text Changded");
                },
              ),
            ),

            Padding(
              padding: EdgeInsets.only(top: 10, bottom: 10),
              child: TextField(
                controller: descriptionController,
                style: textStyle,
                decoration: InputDecoration(
                    labelText: 'Description',
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(5)
                    )
                ),
                onChanged: (value){
                  debugPrint("Text Changded");
                },
              ),
            ),

            Padding(
              padding: EdgeInsets.only(top: 10),
              child: Row(
                children: <Widget>[
                  Expanded(
                    child: RaisedButton(
                      color: Theme.of(context).primaryColorDark,
                      textColor: Theme.of(context).primaryColorLight,
                      elevation: 5,
                      child: Text(
                        "Save",
                        textScaleFactor: 1.5,
                      ),
                      onPressed: (){
                        debugPrint("Submit button pressed");
                      },
                    )
                  ),

                  Container(width: 5),

                  Expanded(
                      child: RaisedButton(
                        elevation: 5,
                        color: Theme.of(context).primaryColorDark,
                        textColor: Theme.of(context).primaryColorLight,
                        child: Text(
                          "Delete",
                          textScaleFactor: 1.5,
                        ),
                        onPressed: (){
                          debugPrint("Delete button pressed");
                        },
                      )
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

}