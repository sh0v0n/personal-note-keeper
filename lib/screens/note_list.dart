import 'package:flutter/material.dart';

class NoteList extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return NoteListState();
  }

}

class NoteListState extends State<NoteList>{
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("My Personal Note Keeper"),
      ),
      body: getNoteListView(),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: (){
          debugPrint("FAB clicked");
        }
      ),
    );
  }

  ListView getNoteListView(){
    int _itemCount = 1;
    TextStyle titleStyle = Theme.of(context).textTheme.subhead;

    return ListView.builder(
      itemCount: _itemCount,
      itemBuilder: (BuildContext context, int item) {
        return Card(
          child: ListTile(
            leading: CircleAvatar(
              backgroundColor: Colors.yellow,
              child: Icon(Icons.arrow_right),
            ),
            title: Text("Dummy Text", style: titleStyle,),
            subtitle: Text("Sub Dummy text"),
            trailing: Icon(Icons.delete),
            onTap: (){
              debugPrint("Tile Clicked");
            },
          ),
          color: Colors.white,
          elevation: 2,

        );
      }
    );
  }
}